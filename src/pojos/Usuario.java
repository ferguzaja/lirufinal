/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojos;

/**
 *
 * @author ferguzaja
 */
public class Usuario {
    private Integer idUsuario;
    private Integer idEmpleado;
    private String usuario;
    private String contraseña;
    private Integer tipoEmpleado;

    public Usuario(Integer idUsuario, Integer idEmpleado, String nombre, String contraseña, Integer tipoEmpleado) {
        this.idUsuario = idUsuario;
        this.idEmpleado = idEmpleado;
        this.usuario = nombre;
        this.contraseña = contraseña;
        this.tipoEmpleado = tipoEmpleado;
    }

    public Usuario(Integer idEmpleado, String nombre, String contraseña, Integer tipoEmpleado) {
        this.idEmpleado = idEmpleado;
        this.usuario = nombre;
        this.contraseña = contraseña;
        this.tipoEmpleado = tipoEmpleado;
    }

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Integer getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getNombre() {
        return usuario;
    }

    public void setNombre(String nombre) {
        this.usuario = nombre;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public Integer getTipoEmpleado() {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(Integer tipoEmpleado) {
        this.tipoEmpleado = tipoEmpleado;
    }
    
    
    
    
    
}
