/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojos;

import java.util.Date;

/**
 *
 * @author ferguzaja
 */
public class Pedido {
    private Integer idPedido;
    private Integer idCliente;
    private String fecha;
    private String estatus;
    private Float total;

    public Pedido(Integer idPedido, Integer idCliente, String fecha, String estatus, Float total) {
        this.idPedido = idPedido;
        this.idCliente = idCliente;
        this.fecha = fecha;
        this.estatus = estatus;
        this.total = total;
    }

    public Pedido( String fecha) {
        this.fecha = fecha;
    }

    public Pedido(Integer idPedido, String fecha) {
        this.idPedido = idPedido;
        this.fecha = fecha;
    }


    public Pedido(Integer idCliente, String fecha, String estatus, float total) {
        this.idCliente = idCliente;
        this.fecha = fecha;
        this.estatus = estatus;
        this.total = total;
    }

    public Integer getIdPedido() {
        return idPedido;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
    
    
    
    
}
