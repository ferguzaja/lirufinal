/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojos;

/**
 *
 * @author ferguzaja
 */
public class Cliente {
    private Integer idCliente;
    private String nombre;
    private String apellidos;
    private String direccion;
    private String email;
    private String telefono;
    private String CP;
    private String ciudad;

    public Cliente() {
    }

    public Cliente(Integer idCliente, String nombre, String apellidos, String direccion, String email, String telefono, String CP, String ciudad) {
        this.idCliente = idCliente;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.email = email;
        this.telefono = telefono;
        this.CP = CP;
        this.ciudad = ciudad;
    }

    public Cliente(String nombre, String apellidos, String direccion, String email, String telefono, String CP, String ciudad) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.email = email;
        this.telefono = telefono;
        this.CP = CP;
        this.ciudad = ciudad;
    }

    public Integer getIdCliente() {
        return idCliente;
    }
    
    
    
    

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCP() {
        return CP;
    }

    public void setCP(String CP) {
        this.CP = CP;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
    
    
    
    
    
}
