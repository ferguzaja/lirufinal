/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojos;

/**
 *
 * @author ferguzaja
 */
public class Pedido_Producto {
    private Integer idPedido_Producto;
    private Integer idPedido;
    private Integer idProducto;
    private int cantidad;
    private float precioActual;

    public Pedido_Producto() {
    }

    public Pedido_Producto(Integer idPedido) {
        this.idPedido = idPedido;
    }
    

    public Pedido_Producto(Integer idPedido_Producto, Integer idPedido, Integer idProducto, int cantidad, float precioActual) {
        this.idPedido_Producto = idPedido_Producto;
        this.idPedido = idPedido;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.precioActual = precioActual;
    }

    public Pedido_Producto(Integer idPedido, Integer idProducto, int cantidad, float precioActual) {
        this.idPedido = idPedido;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.precioActual = precioActual;
    }
    
    
    
    

    public Integer getIdPedido_Producto() {
        return idPedido_Producto;
    }


    public Integer getIdPedido() {
        return idPedido;
    }


    public Integer getIdProducto() {
        return idProducto;
    }


    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecioActual() {
        return precioActual;
    }

    public void setPrecioActual(float precioActual) {
        this.precioActual = precioActual;
    }
    
    
    
    
}
